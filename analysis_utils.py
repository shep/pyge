
#TODO: contig name conversion
#TODO: Optional and non-specific searches
#TODO: VEP data parsing
def gnomad_annotate(contig, pos, allele0, allele1, rsid=None, end=None):
    import pysam

    REF="/groups/nshomron/guyshapira/data/gnomad3.vcf.bgz"

    vcf=pysam.VariantFile(REF, threads=24)

    vs=vcf.fetch(contig=contig, start=pos-1, end=pos+len(allele1))

    vc=None
    for v in vs:
        if rsid:
            if v.id == rsid:
                vc=v
                break
        if v.pos == pos and v.alleles[0] == allele0 and v.alleles[1] == allele1:
            vc=v
            break

    if vc:
        return { k:vc.info.get(k) for k in vc.info.keys()}
    else:
        return None
