'''
Argument: 1.) Species
'''
import sys
sys.path.insert(0,'/groups/nshomron/guyshapira/projects/pyge/')
import sequencing_utils as sutils
import pandas as pd
import os

sam=pd.read_csv("samples.csv",index_col=0)


pipe={}

for i,l in sam.iterrows():
    r1=os.path.join(i,i + "_R1.fastq.gz")
    r2=os.path.join(i,i + "_R2.fastq.gz")

    # Raw reads QC
    j=sutils.fastQC([r1, r2])
    j.name="raw_qc_" + i
    j.execute()
    pipe[i]={"raw_qc":j}

    # Trimming
    t1=r1.replace(".fastq.gz", ".trimmed.fastq.gz")
    t2=r2.replace(".fastq.gz", ".trimmed.fastq.gz")

    j=sutils.trimReads(r1=r1, r2=r2, t1=t1, t2=t2,
                       report_json=os.path.join(i,i + ".fastp.json"),
                       report_html=os.path.join(i,i + ".fastp.html"))
    j.name="trim_" + i
    j.execute()
    pipe[i]["trimming"]=j

    # Trimmed reads QC
    j=sutils.fastQC([t1, t2])
    j.dependencies=[pipe[i]["trimming"].jid]
    j.name="trimmed_qc_" + i
    j.execute()
    pipe[i]["trimmed_qc"]=j

    # Align
    j=sutils.alignRNAseq(species=sys.argv[1], r1=t1, r2=t2, out_dir=i)
    j.dependencies=[pipe[i]["trimming"].jid]
    j.name="align" + i
    j.execute()
    pipe[i]["alignment"]=j

# Merge counts
deps=[ pipe[i]["alignment"].jid for i in sam.index ]

j=sutils.mergeCounts()
j.name="merge_counts"
j.dependencies=deps
j.execute()


# Merge quality control
deps=[ pipe[i]["trimmed_qc"].jid for i in sam.index ]
deps.extend([ pipe[i]["alignment"].jid for i in sam.index ])

j=sutils.multiQC()
j.name="merge_qc"
j.dependencies=deps
j.execute()

