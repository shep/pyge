import sys
sys.path.insert(0,'/groups/nshomron/guyshapira/projects/pyge/')
from job import Job
import pandas as pd
import os


class salmonQuant(Job) :

    def __init__(self, species, r1, r2, out_dir, **kwargs):
        JOB_NAME="salmon"
        self.species=species
        self.out_dir=out_dir
        self.r1=r1
        self.r2=r2

        if self.species == "human":
            self.index="/groups/nshomron/guyshapira/data/salmon/pre_packed_human"
        elif self.species == "mouse":
            self.index="/groups/nshomron/guyshapira/data/salmon/mouse_new"

        THREADS=24
        MEM="25G"

        cmd=["/groups/nshomron/guyshapira/bin/salmon", "quant",
             "-i", "/groups/nshomron/guyshapira/data/salmon/pre_packed_human",
             "--gcBias", "--seqBias", "--validateMappings", "--allowDovetail", "--rangeFactorizationBins", "4",
             "-l", "ISR", "-p", str(THREADS),
             "-1", "<(gunzip -c {0})".format(r1),
             "-2", "<(gunzip -c {0})".format(r2),
             "-o", "{0}".format(out_dir)]

        super(salmonQuant, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)




class countFeats(Job):

    def __init__(self, in_bam, out_file, species="human", unique_only=True, transcript_level=False, stranded="reverse", **kwargs):
        JOB_NAME="counts"
        APP="/groups/nshomron/guyshapira/.local/bin/htseq-count"
        MEM="10G"
        THREADS=24

        gtf=None
        if species == "human":
            gtf="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.100.chr_patch_hapl_scaff.gtf"
        elif species == "mouse":
            gtf="/groups/nshomron/guyshapira/data/genome/Mus_musculus.GRCm38.93.chr_patch_hapl_scaff.gtf"

        cmd=["module load python/python-3.5.1;",
            APP,
            "-f", "bam",
            "-s", stranded,
            in_bam,
            gtf
            ]

        if transcript_level:
            cmd.extend(["--idattr transcript_id --nonunique none --secondary-alignments ignore --supplementary-alignments ignore"])

        cmd.extend([">",out_file])

        super(countFeats, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)



class trimReads(Job):

    def __init__(self, r1, r2=None, t1=None, t2=None, report_json=None, report_html=None, **kwargs):
        JOB_NAME="Trim"
        APP="/groups/nshomron/guyshapira/bin/fastp"
        MEM="10G"
        THREADS=24

        if t1 is None:
            t1=r1.replace(".fastq.gz", ".trimmed.fastq.gz")

        if r2 is not None and t2 is None:
                t2=r2.replace(".fastq.gz", ".trimmed.fastq.gz")

        if report_html is None:
            report_html=r1 + ".html"

        if report_json is None:
            report_json=r1 + ".json"



        cmd=[APP, "-i", r1, "-o", t1,
            "-j", report_json,
            "-h", report_html,
            "--thread", str(THREADS)]

        # If paired-end
        if r2 is not None:
            cmd.extend(["-I", r2, "-O", t2, "--detect_adapter_for_pe"])

        super(trimReads, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class fastQC(Job):

    def __init__(self, seqfiles, **kwargs):
        JOB_NAME="fastqc"
        APP="/groups/nshomron/guyshapira/bin/fastqc"
        MODULE="java/jdk-11.0.2"
        MEM="35G"

        THREADS=24

        cmd=["module", "load", MODULE + ";",
            APP, "--threads", str(THREADS)]

        cmd.extend(seqfiles)

        super(fastQC, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class align2Ref(Job):

    def __init__(self, r1, r2, out_bam, index="GRCh38", **kwargs):
        JOB_NAME="Align"
        APP="/groups/nshomron/guyshapira/tools/bowtie2/bowtie2"
        SAPP="/groups/nshomron/guyshapira/bin/samtools"
        THREADS=24
        MEM="35G"

        idx="/groups/nshomron/guyshapira/data/bowtie2_index/" + index

        #TODO: Single-end support
        cmd=[APP, "-x", idx,
            "-1", r1, "-2", r2,
            "-p", str(THREADS),
            "|", SAPP, "view", "-bS", "-",
            ">", out_bam]

        super(align2Ref, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class alignRNAseq(Job) :

    def __init__(self, species, r1, out_dir, r2=None, **kwargs):
        JOB_NAME="staralign"

        if species == "human":
            index="/groups/nshomron/guyshapira/data/star_index/human_grch38_r92"
            ref="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.94.chr_patch_hapl_scaff.gtf"
        elif species == "mouse":
            index="/groups/nshomron/guyshapira/data/star_index/mouse"
            ref="/groups/nshomron/guyshapira/data/genome/Mus_musculus.GRCm38.93.chr_patch_hapl_scaff.gtf"

        THREADS=20
        MEM="50G"

        # Trim raw sequencing data
        cmd=["/groups/nshomron/guyshapira/bin/STAR",
            "--runThreadN", str(THREADS),
            "--genomeDir", index,
            "--genomeLoad", "NoSharedMemory",
            "--outSAMtype", "BAM", "Unsorted",
            "--outFileNamePrefix", out_dir + "/",
            "--quantMode", "GeneCounts",
            "--readFilesCommand", "zcat",
            "--readFilesIn"]

        if type(r1) is list:
            cmd.append(",".join(r1))
        else:
            cmd.append(r1)

        if r2:
            if type(r2) is list:
                cmd.append(",".join(r2))
            else:
                cmd.append(r2)

        super(alignRNAseq, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)



class markDuplicates(Job) :

    def __init__(self, bam_in, bam_out, metrics_file=None, TMP_DIR="/groups/nshomron/guyshapira/tmp2/tmp", **kwargs):
        JOB_NAME="markDups"
        APP="/groups/nshomron/guyshapira/bin/gatk"
        THREADS=24
        MEM="40G"
        vm_mem="20G"
        MODULE="java/jdk-11.0.2"


        if metrics_file is None:
            metrics_file=bam_out + ".metrics.txt"


        cmd=["module load", MODULE + ";",
             APP,
            "--java-options", "-Xmx" + vm_mem,
            "MarkDuplicates",
            "--INPUT", bam_in,
            "--OUTPUT", bam_out,
            "--METRICS_FILE", metrics_file,
            "--TMP_DIR", TMP_DIR]

        super(markDuplicates, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)




class addReadGroups(Job) :

    def __init__(self, bam_in, out_bam, sample_name, library_name="lib1",
                 platform="ILLUMINA", platform_unit="unit1", group_id=None,
                 TMP_DIR="/groups/nshomron/guyshapira/tmp2/tmp", **kwargs):
        JOB_NAME="addReadGroups"
        THREADS=24
        MEM="25G"
        vm_mem="10G"


        cmd=["module load java/jdk-11.0.2;",
            "/groups/nshomron/guyshapira/bin/gatk",
            "--java-options", "-Xmx" + vm_mem,
            "AddOrReplaceReadGroups",
            "--INPUT", bam_in,
            "--OUTPUT", out_bam,
            "--RGSM", sample_name,
            "--RGLB", library_name,
            "--RGPU", platform_unit,
            "--RGPL", platform,
            "--TMP_DIR", TMP_DIR]
            # "--CREATE_INDEX"]

        if group_id:
            cmd.extend(["--RGID", group_id])

        super(addReadGroups, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class splitNCigar(Job) :

    def __init__(self, bam_in, reference, out_bam, **kwargs):
        JOB_NAME="splitNCigar"
        THREADS=20
        MEM="20G"
        vm_mem="10G"


        cmd=["module load java/jdk-11.0.2;",
            "/groups/nshomron/guyshapira/bin/gatk",
            "--java-options", "-Xmx" + vm_mem,
            "SplitNCigarReads",
            "--input", bam_in,
            "--reference", reference,
            "--output", out_bam]

        super(splitNCigar, self).__init__(cmd=" ".join(cmd),memory_limit=MEM, **kwargs)


class haplotypeCaller(Job) :

    def __init__(self, bam_in, reference, out_file, mode="GVCF", min_phred=30.0, **kwargs):
        JOB_NAME="hapCaller"
        THREADS=24

        vm_mem="10G"
        MEM="25G"

        cmd=["module load java/jdk-11.0.2;",
            "/groups/nshomron/guyshapira/bin/gatk",
            "--java-options", "-Xmx" + vm_mem,
            "HaplotypeCaller",
            "--input", bam_in,
            "--reference", reference,
            "-ERC", mode,
            "--standard-min-confidence-threshold-for-calling", str(min_phred),
            "--output", out_file]

        #NOTE: It does it automatically, allegedly
        # "--native-pair-hmm-threads", str(self.THREADS),

        super(haplotypeCaller, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class gvcfs2DB(Job) :

    def __init__(self, gvcfs, reference, intervals, db_dir="genomicDB", update_db=False,
            vcf_buffer_size=1638400, db_segment_size=10085760,
            tmp_dir="/genomicslab/nobackup/volume2/guyshapira/tmp", **kwargs):
        JOB_NAME="cmbgvcfs"

        THREADS=20
        MEM="55G"
        vm_mem="50G"


        cmd=["module load java/jdk-11.0.2;",
            "/groups/nshomron/guyshapira/bin/gatk",
            "--java-options", "-Xmx" + vm_mem,
            "GenomicsDBImport",
            "--merge-input-intervals",
            "--reference", reference,
            "--genomicsdb-vcf-buffer-size", str(vcf_buffer_size),
            "--genomicsdb-segment-size", str(db_segment_size),
            "--max-num-intervals-to-import-in-parallel", str(THREADS),
            "--intervals", intervals,
            "--tmp-dir=" + tmp_dir
            ]

        if update_db:
            cmd.extend(["--genomicsdb-update-workspace-path", db_dir])
        else:
            cmd.extend(["--genomicsdb-workspace-path", db_dir])

        cmd.extend([ "--variant " + g for g in gvcfs ])

        super(gvcfs2DB, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class genotypeGVCFs(Job) :

    def __init__(self, reference, gdb="genomicDB", out_file="cohort.vcf.gz", tmp_dir="/genomicslab/nobackup/volume2/guyshapira/tmp", **kwargs):
        JOB_NAME="gengvcfs"

        THREADS=24
        MEM="75G"
        vm_mem="60G"

        cmd=["module load java/jdk-11.0.2;",
            "/groups/nshomron/guyshapira/bin/gatk",
            "--java-options", "-Xmx" + vm_mem,
            "GenotypeGVCFs",
            "--reference", reference,
            "--variant", "gendb://" + gdb,
            "--output", out_file]

        if not tmp_dir is None:
            cmd.append("--tmp-dir=" + tmp_dir)

        super(genotypeGVCFs, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class mergeCounts(Job):

    def __init__(self, **kwargs):
        JOB_NAME="mergeCounts"


        cmd=["module load python/python-3.5.1;",
             "/share/apps/python/python351/bin/python",
             "/groups/nshomron/guyshapira/tools/rnaseq/joinCounts.py"]

        super(mergeCounts, self).__init__(cmd=" ".join(cmd), **kwargs)



# class combineGVCFs(Job) :
#
#     def __init__(self, gvcfs, reference, out_file):
#         JOB_NAME="cmbgvcfs"
#         self.gvcfs=gvcfs
#         self.reference=reference
#         self.out_file=out_file
#
#         self.THREADS=20
#         self.MEM="50G"
#         self.vm_mem="40G"
#
#
#         cmd=["module load java/jdk-11.0.2;",
#             "/groups/nshomron/guyshapira/bin/gatk",
#             "--java-options", "-Xmx" + self.vm_mem,
#             "CombineGVCFs",
#             "--reference", self.reference,
#             "--output", self.out_file]
#
#         cmd.extend([ "--variant " + g for g in self.gvcfs ])
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#
#
# """
# Funcotator
# ----------
#
# """
# class Funcotator(Job) :
#
#     def __init__(self, vcf, reference, out_file, out_format="VCF", ref_version="hg38", data_path="/groups/nshomron/guyshapira/data/funcotator"):
#         JOB_NAME="func"
#         self.vcf=vcf
#         self.reference=reference
#         self.out_file=out_file
#         self.out_format=out_format
#         self.ref_version=ref_version
#         self.data_path=data_path
#         self.THREADS=20
#         self.MEM="50G"
#         self.vm_mem="40G"
#
#         cmd=["module load java/jdk-11.0.2;",
#             "/groups/nshomron/guyshapira/bin/gatk",
#             "--java-options", "-Xmx" + self.vm_mem,
#             "Funcotator",
#             "--variant", self.vcf,
#             "--reference", self.reference,
#             "--output", self.out_file,
#             "--output-file-format", self.out_format,
#             "--ref-version", self.ref_version,
#             "--data-sources-path", self.data_path
#             ]
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#
#
#
# class alignRNAseq2Pass(Job) :
#
#     def __init__(self, species, r1, r2, out_dir):
#         JOB_NAME="star2pass"
#         self.species=species
#         self.out_dir=out_dir
#         self.r1=r1
#         self.r2=r2
#
#         if self.species == "human":
#             self.index="/groups/nshomron/guyshapira/data/star_index/human_grch38_r92"
#             self.ref="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.94.chr_patch_hapl_scaff.gtf"
#         elif self.species == "mouse":
#             self.index="/groups/nshomron/guyshapira/data/star_index/mouse"
#             self.ref="/groups/nshomron/guyshapira/data/genome/Mus_musculus.GRCm38.93.chr_patch_hapl_scaff.gtf"
#
#         self.THREADS=20
#         self.MEM="50G"
#
#         # Trim raw sequencing data
#         cmd=["/groups/nshomron/guyshapira/bin/STAR",
#             "--runThreadN", str(self.THREADS),
#             "--genomeDir", self.index,
#             "--readFilesIn", self.r1, self.r2,
#             "--genomeLoad", "NoSharedMemory",
#             "--outSAMtype", "BAM", "Unsorted",
#             "--outFileNamePrefix", self.out_dir + "/",
#             "--quantMode", "GeneCounts",
#             "--twopassMode", "Basic",
#             "--readFilesCommand", "zcat"]
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#
#
#
#

#
# class alignDirtyRNAseq(Job) :
#
#     def __init__(self, species, r1, r2, out_dir):
#         JOB_NAME="stardirtalign"
#         self.species=species
#         self.out_dir=out_dir
#         self.r1=r1
#         self.r2=r2
#
#         if self.species == "human":
#             self.index="/groups/nshomron/guyshapira/data/star_index/human_grch38_r92"
#             self.ref="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.94.chr_patch_hapl_scaff.gtf"
#         elif self.species == "mouse":
#             self.index="/groups/nshomron/guyshapira/data/star_index/mouse"
#             self.ref="/groups/nshomron/guyshapira/data/genome/Mus_musculus.GRCm38.93.chr_patch_hapl_scaff.gtf"
#
#         self.THREADS=20
#         self.MEM="50G"
#
#         # Trim raw sequencing data
#         cmd=["/groups/nshomron/guyshapira/bin/STAR",
#             "--runThreadN", str(self.THREADS),
#             "--genomeDir", self.index,
#             "--readFilesIn", self.r1, self.r2,
#             "--genomeLoad", "NoSharedMemory",
#             "--outSAMtype", "BAM", "Unsorted",
#             "--outReadsUnmapped", "Fastx",
#             "--outFileNamePrefix", self.out_dir + "/",
#             "--outFilterScoreMinOverLread", "0.3",
#             " --outFilterMatchNminOverLread", "0.3",
#             "--quantMode", "GeneCounts",
#             "--readFilesCommand", "zcat"]
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#

#TODO: Implement with sambamba
class sortBam(Job):

    def __init__(self, in_bam, out_bam=None, **kwargs):
        JOB_NAME="sortBam"
        THREADS=10
        MEM="50G"
        APP="/groups/nshomron/guyshapira/bin/samtools"

        if out_bam:
            self.out_bam=out_bam
        else:
            self.out_bam=in_bam.replace(".bam", ".sorted.bam")


        # Trim raw sequencing data
        cmd=[APP,
            "sort",
            "--threads", str(THREADS),
            "-m", "1G",
            "-o", out_bam,
            in_bam]

        super(sortBam, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


#TODO: Implement with sambamba
class indexBam(Job):

    def __init__(self, in_bam, **kwargs):
        JOB_NAME="indexBam"
        THREADS=20

        tMEM="10G"

        APP="/groups/nshomron/guyshapira/bin/samtools"

        # Trim raw sequencing data
        cmd=[APP,
            "index",
            "-@", str(THREADS),
            in_bam]

        super(indexBam, self).__init__(cmd=" ".join(cmd), memory_limit="25G", **kwargs)


class multiQC(Job):

    def __init__(self, path=".", **kwargs):
        JOB_NAME="multiqc"
        MEM="30G"


        cmd=["module load python/python-3.5.1;",
             "/share/apps/python/python351/bin/python",
             "/groups/nshomron/guyshapira/.local/bin/multiqc",
             path]

        super(multiQC, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)

class ensemblVEP(Job):

    def __init__(self, in_vcf, out_vcf=None, assembly="GRCh38", **kwargs):
        JOB_NAME="annbcf"

        if out_vcf is None:
            out_vcf=os.path.join(os.path.dirname(in_vcf), os.path.basename(in_vcf).replace(".vcf.gz",".annotated.vcf.gz"))

        THREADS=20
        MEM="45G"
        tMEM="25G"

        APP="/groups/nshomron/guyshapira/vep/ensembl-vep/vep"
        cmd=["module load perl/perl-5.28;",
            APP,
            "--merged",
            "--assembly", assembly,
            "--input_file", in_vcf,
            "--output_file", out_vcf,
            "--no_stats", "--offline",
            "--fork", str(THREADS),
            "--pick_allele --compress_output bgzip",
            "--vcf --sift b --polyphen b --variant_class --gene_phenotype --regulatory --hgvs --symbol --biotype --af",
            "--synonyms", "/groups/nshomron/guyshapira/tools/chrom_synonyms.txt",
            "--fasta", "/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.dna.toplevel.fa",
            "--plugin", "CADD,/groups/nshomron/guyshapira/tmp/cadd/whole_genome_SNVs.tsv.gz,/groups/nshomron/guyshapira/tmp/cadd/gnomad.genomes.r3.0.indel.tsv.gz",
            "--plugin", "dbNSFP,/groups/nshomron/guyshapira/tools/snpEff/snpEff/data/GRCh38/dbNSFP/dbNSFP4.0a.txt.gz,ALL"
            ]

        super(ensemblVEP, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)

class bcfFilter(Job):

    def __init__(self, in_vcf, out_vcf, exprs, **kwargs):
        JOB_NAME="filterbcf"

        THREADS=20
        MEM="25G"
        tMEM="10G"

        APP="/groups/nshomron/guyshapira/bin/bcftools"

        cmd=[APP,
            "filter",
            "--threads", str(THREADS),
            "--output-type", "z",
            "--output", out_vcf,
            exprs,
            in_vcf]

        super(bcfFilter, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)

# Format VCFs as biallelics
class bcfNorm(Job):

    def __init__(self, in_vcf, out_vcf, **kwargs):
        JOB_NAME="normbcf"

        THREADS=20
        MEM="25G"
        tMEM="10G"

        APP="/groups/nshomron/guyshapira/bin/bcftools"

        cmd=[APP,
            "norm",
            "--threads", str(THREADS),
            "--output-type", "z",
            "--output", out_vcf,
            "--multiallelics", "-",
            in_vcf]

        super(bcfNorm, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


# Gemma association analyis
class gemmaModeling(Job):

    def __init__(self, in_plink, out_res=None, model="lmm", **kwargs):
        JOB_NAME="gemma"

        THREADS=20
        MEM="25G"
        tMEM="10G"

        APP="/groups/nshomron/guyshapira/bin/gemma"
        cmd=[]
        if not out_res:
            out_res=model + "_" + in_plink

        if model == "lmm":
            cmd.extend([APP, "-bfile", in_plink, "-gk", "1", "-o", "rel_mat" + ";"])

        # gemma -bfile cmv -k output/rel_mat.cXX.txt -lmm 4 -o lmm_cmv
        cmd.extend([
            APP,
            "-bfile",
            in_plink,
            "-" + model, "4",
            "-o", out_res])

        if model == "lmm":
            cmd.extend(["-k", "output/rel_mat.cXX.txt"])

        super(gemmaModeling, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)



# Format VCF to plink
class vcf2Plink(Job):

    def __init__(self, in_vcf, out_prefix, **kwargs):
        JOB_NAME="vcf2plink"

        THREADS=20
        MEM="25G"
        tMEM="10G"

        APP="/groups/nshomron/guyshapira/bin/plink"

        # plink --vcf cmv.norm.vcf.gz --make-bed --out cmv --allow-extra-chr
        cmd=[APP,
            "--vcf",
            in_vcf,
            "--make-bed",
            "--out", out_prefix,
            "--allow-extra-chr"]

        super(vcf2Plink, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)


class vepVCF2CSV(Job):

    def __init__(self, in_vcf, out_vcf=None, **kwargs):
        JOB_NAME="vcf2csv"

        if out_vcf is None:
            out_vcf=os.path.join(os.path.dirname(in_vcf), os.path.basename(in_vcf).replace(".vcf.gz",".annotated.csv"))

        THREADS=20
        MEM="45G"

        cmd=["module load python/python-3.5.1;",
            "/share/apps/python/python351/bin/python3",
            "/groups/nshomron/guyshapira/tools/vcf2csv.py",
            in_vcf,
            out_vcf
            ]

        super(vepVCF2CSV, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)

class bcfIndex(Job):

    def __init__(self, in_vcf, **kwargs):
        JOB_NAME="indexbcf"

        in_vcf=in_vcf

        THREADS=20
        MEM="35G"
        tMEM="25G"

        APP="/groups/nshomron/guyshapira/bin/bcftools"

        cmd=[APP,
            "index",
            "--threads", str(THREADS),
            "--tbi",
            in_vcf]

        super(bcfIndex, self).__init__(cmd=" ".join(cmd), memory_limit=MEM, **kwargs)




# class bcfMerge(Job):
#
#     def __init__(self, in_vcfs, out_vcf="merged.vcf.gz"):
#         JOB_NAME="mergebcf"
#
#         self.in_vcfs=in_vcfs
#         self.out_vcf=out_vcf
#
#         self.THREADS=20
#         self.MEM="25G"
#         self.tMEM="10G"
#
#         APP="/groups/nshomron/guyshapira/bin/bcftools"
#
#         cmd=[APP,
#             "merge",
#             "--threads", str(self.THREADS),
#             "--output-type", "z",
#             "--output", self.out_vcf
#             ]
#
#         cmd.extend(self.in_vcfs)
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#
#

#
# class bcfAnnotate(Job):
#
#     def __init__(self, in_vcf, out_vcf=None, annotation_file="/groups/nshomron/guyshapira/tmp/dbsnp/human_9606_b151_GRCh38p7.vcf.gz", exprs=""):
#         JOB_NAME="annbcf"
#
#         self.in_vcf=in_vcf
#         self.exprs=exprs
#         if out_vcf:
#             self.out_vcf=out_vcf
#         else:
#             self.out_vcf=os.path.join(os.path.dirname(in_vcf), os.path.basename(in_vcf).replace(".vcf.gz","annotated.vcf.gz"))
#
#         self.THREADS=20
#         self.MEM="45G"
#         self.tMEM="25G"
#
#         APP="/groups/nshomron/guyshapira/bin/bcftools"
#
#         cmd=[APP,
#             "annotate",
#             "--annotations", annotation_file,
#             "--threads", str(self.THREADS),
#             "--output-type", "z",
#             "--output", self.out_vcf,
#             "--columns", "ID,INFO",
#             self.exprs,
#             self.in_vcf]
#
#         Job.__init__(self, cmd=" ".join(cmd), memory_limit=self.MEM, name=JOB_NAME)
#
#


#
#
#
#
# #TODO: Doesn't work
# # class removeEmptyLines(Job):
# #
# #     def __init__(self, f):
# #
# #
# #         cmd=["zcat", f,
# #              "| grep \"\S\" > ", f.replace(".gz","")]
# #
# #         Job.__init__(self, cmd=" ".join(cmd))
#
