import subprocess,re,os
from datetime import datetime
from pathlib import Path
import random

SUBMITTER="qsub"
JID_REGEX="Your job ([0-9]+) "
LOG_FOLDER=".logs"
TEMP_FOLDER="scripts"
JOB_NAME_VAR="$JOB_NAME"
JOB_ID_VAR="$JOB_ID"


class Job():
    """
    This class represents an SGE job.
    """
    def __init__(self, cmd=None, name="job", dependencies=[], args=None, memory_limit=None,
                 stdout=None, stderr=None, available_nodes="compute-0-[3-9]|compute-0-1[0-5]",
                 shell="/bin/bash", cwd=None, priority=-10,
                 print_cmd=False, print_sub=True, print_reply=True):
        """
        SGE job constructor.

        Paramaters:
            cmd (str): Bash script string, to be excecuted as a SGE job.
            args (list): Command line arguments to be passed to qsub.
            name (str): Name for the job.
            dependencies (list): IDs of jobs dependent for excecution.
            available_nodes (str): A regex expression defining nodes available for execution. None=All.
            ...
        """
        self.name = name
        self.cmd=cmd
        self.args=args
        self.dependencies=dependencies
        self.memory_limit=memory_limit
        self.stdout=stdout
        self.stderr=stderr
        self.shell=shell
        self.cwd=cwd
        self.priority=priority
        self.available_nodes=available_nodes
        self.print_cmd=print_cmd
        self.print_sub=print_sub
        self.print_reply=print_reply

    def execute(self):

        #TODO: Better naming scheme
        self.uid=".".join([self.name, str(random.randint(0,1024)), str(os.getpid())])

        # Construct excecution string with arguments
        sub=[]

        #TODO: Does cwd needs to be dealt with? how?
        sub.append("-cwd")

        if self.stdout is None:
            self.stdout=os.path.join(LOG_FOLDER, JOB_NAME_VAR + "." + JOB_ID_VAR + ".OUT")
            sub.extend(["-o", self.stdout])
        if self.stderr is None:
            self.stderr=os.path.join(LOG_FOLDER, JOB_NAME_VAR + "." + JOB_ID_VAR + ".ERR")
            sub.extend(["-e", self.stderr])
        if self.shell is not None:
            sub.extend(["-S", self.shell])
        if self.name is not None:
            sub.extend(["-N", self.name])
        if self.memory_limit:
            sub.extend(["-l", "h_vmem={0}".format(self.memory_limit)])
        if self.available_nodes:
            #TODO: Friendlier control
            #'compute-0-[2-9]|compute-0-1[0-5]'
            sub.extend(["-l", "nsh,h=" + self.available_nodes])
        if self.priority is not None:
            sub.extend(["-p", str(self.priority)])
        if type(self.dependencies) is list:
            if len(self.dependencies) > 0:
                sub.extend(["-hold_jid", ",".join([ str(d) for d in self.dependencies])])
        else:
            sub.extend(["-hold_jid", str(self.dependencies)])


        # Add job excecution strings
        self.exc=[SUBMITTER]
        self.exc.extend(sub)

        # Create log and temp. folders if none exist
        log_folder=LOG_FOLDER
        temp_folder=os.path.join(LOG_FOLDER, TEMP_FOLDER)
        if not os.path.exists(log_folder):
            os.mkdir(log_folder)
        if not os.path.exists(temp_folder):
            os.mkdir(temp_folder)

        # Add submission arguments, if any
        if self.args:
            self.exc.extend(self.args)

        # Create batchfile according to cmd argument
        batchfile=os.path.join(temp_folder, ".".join([self.uid, "sh"]))
        with open(batchfile, "w") as f:
            f.write(self.cmd)
        self.exc.append(batchfile)

        self.returned=subprocess.Popen(self.exc, stdout=subprocess.PIPE)
        self.reply=str(self.returned.stdout.read())
        try:
            self.jid=int(re.search("Your job ([0-9]+) ", self.reply).group(1))
        except:
            self.jid=0

        # Debug prints
        if self.print_cmd:
            print(self.cmd)

        if self.print_sub:
            print(" ".join(self.exc))

        if self.print_reply:
            print(self.reply)

    def __str__(self):
        if self.returned:
            return str(self.jid) + " " + " ".join(self.exc)
        else:
            return "Not running " + " ".join(self.exc)
