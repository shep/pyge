'''
Argument: 1.) Species
'''
import sys
sys.path.insert(0,'/groups/nshomron/guyshapira/projects/pyge/')
import sequencing_utils as sutils
import pandas as pd
import os

sam=pd.read_csv("samples.csv",index_col=0)

species=sys.argv[1]

pipe={}

ref="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.dna.primary_assembly.fa"

gvs=[]
for i,l in sam.iterrows():
    r1=os.path.join(i,i + "_R1.fastq.gz")
    r2=os.path.join(i,i + "_R2.fastq.gz")

    # Raw reads QC
    j=sutils.fastQC([r1, r2])
    j.name="raw_qc_" + i
    j.execute()
    pipe[i]={"raw_qc":j}

    # Trimming
    t1=r1.replace(".fastq.gz", ".trimmed.fastq.gz")
    t2=r2.replace(".fastq.gz", ".trimmed.fastq.gz")

    j=sutils.trimReads(r1=r1, r2=r2, t1=t1, t2=t2,
                       report_json=os.path.join(i,i + ".fastp.json"),
                       report_html=os.path.join(i,i + ".fastp.html"))
    j.name="trim_" + i
    j.execute()
    pipe[i]["trimming"]=j

    # Trimmed reads QC
    j=sutils.fastQC([t1, t2])
    j.dependencies=[pipe[i]["trimming"].jid]
    j.name="trimmed_qc_" + i
    j.execute()
    pipe[i]["trimmed_qc"]=j

    # Align
    b=os.path.join(i,i + ".bam")
    j=sutils.align2Ref(species, t1, t2, )
    j.dependencies=[pipe[i]["trimming"].jid]
    j.name="align" + i
    j.execute()
    pipe[i]["alignment"]=j

    # Sort bam
    sb=os.path.join(i,i + ".sorted.bam")
    j=sutils.sortBam(b, sb)
    j.dependencies=[pipe[i]["align"].jid]
    j.name="sort" + i
    j.execute()
    pipe[i]["sort"]=j

    # Index bam
    j=sutils.indexBam(sb)
    j.dependencies=[pipe[i]["sort"].jid]
    j.name="index" + i
    j.execute()
    pipe[i]["index"]=j

    #TODO: Fix mark duplicates
    # Mark duplicates
    # mb=os.path.join(i, i + ".dedup.bam")
    # j=sutils.markDuplicates(sb, mb)
    # j.dependencies=[pipe[i]["index"].jid]
    # j.name="dedup" + i
    # j.execute()
    # pipe[i]["dedup"]=j
    #
    #TODO: Base quality calibration

    # Variant calling per sample
    gvcf=os.path.join(i, i + ".g.vcf.gz")
    gvs.append(gvcf)
    j=sutils.haplotypeCaller(sb, ref, gvcf)
    j.dependencies=[pipe[i]["index"].jid]
    j.name="gcall" + i
    j.execute()
    pipe[i]["gcall"]=j

# Combine GVCFs
cohort_file="cohort.g.vcf.gz"
j=sutils.combineGVCFs(gvs, ref, cohort_file)
j.dependencies=[ pipe[i]["gcall"].jid for i in sam.index]
j.name="ccall" + i
j.execute()
pipe["all_call"]=j

# Consolidate GVCF
vcf_out="cohort.vcf.gz"
j=sutils.genotypeGVCFs(cohort_file, ref, vcf_out)
j.dependencies=[ pipe["all_call"].jid ]
j.name="genocall" + i
j.execute()
pipe["genocall"]=j


