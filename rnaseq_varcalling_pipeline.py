'''
Argument: 1.) Species
'''
import sys
sys.path.insert(0,'/groups/nshomron/guyshapira/projects/pyge/')
import sequencing_utils as su
import pandas as pd
import os

sam=pd.read_csv("samples.csv",index_col=0)

if len(sys.argv) > 1:
    species=sys.argv[1]
else:
    species="human"

pipe={ i:{} for i in sam.index }

ref="/groups/nshomron/guyshapira/data/genome/Homo_sapiens.GRCh38.dna.primary_assembly.fa"

gvs=[]
for i,l in sam.iterrows():
    r1=os.path.join(i,i + "_R1.fastq.gz")
    r2=os.path.join(i,i + "_R2.fastq.gz")

    t1=r1.replace(".fastq.gz", ".trimmed.fastq.gz")
    t2=r2.replace(".fastq.gz", ".trimmed.fastq.gz")

    # Align two-pass
    j=su.alignRNAseq2Pass(species=species, r1=t1, r2=t2, out_dir=i)
    j.name="align" + i
    j.execute()
    pipe[i]["align"]=j

    # Sort bam
    b=os.path.join(i,"Aligned.out.bam")
    sb=os.path.join(i,i + ".sorted.bam")
    j=su.sortBam(b, sb)
    j.dependencies=[pipe[i]["align"].jid]
    j.name="sort" + i
    j.execute()
    pipe[i]["sort"]=j

    # Index bam
    j=su.indexBam(sb)
    j.dependencies=[pipe[i]["sort"].jid]
    j.name="index" + i
    j.execute()
    pipe[i]["index"]=j

    # Mark duplicates
    mb=os.path.join(i, i + ".dedup.bam")
    j=su.markDuplicates(sb, mb)
    j.dependencies=[pipe[i]["index"].jid]
    j.name="dedup" + i
    j.execute()
    pipe[i]["dedup"]=j


    # Add read groups
    bb=os.path.join(i,i + ".prepped.bam")
    j=su.addReadGroups(mb, bb, sample_name=i)
    j.dependencies=[pipe[i]["dedup"].jid]
    j.name="addRG" + i
    j.execute()
    pipe[i]["addRG"]=j

    # Deal with splice-site alignment
    bb2=os.path.join(i,i + ".snc.bam")
    j=su.splitNCigar(bb, ref, bb2)
    j.dependencies=[pipe[i]["addRG"].jid]
    j.name="SnC" + i
    j.execute()
    pipe[i]["SnC"]=j

    # Variant calling per sample
    vcf=os.path.join(i, i + ".vcf.gz")
    j=su.haplotypeCaller(bb2, ref, vcf, "NONE", 18.0)
    j.dependencies=[pipe[i]["SnC"].jid]
    j.name="gcall" + i
    j.execute()
    pipe[i]["gcall"]=j
    gvs.append(vcf)

# Merge per-sample variant calls
o="merged.vcf.gz"
j=su.bcfMerge(gvs, o)
j.dependencies=[ pipe[i]["gcall"].jid for i in pipe.keys() ]
j.execute()

# Rough filtering
of=o.replace(".vcf.gz", ".filtered.vcf.gz")
jj=su.bcfFilter(o, of, "-i 'AVG(QD) > 5 && AVG(FORMAT/DP) > 10 && AVG(MQ) > 20.0'")
jj.dependencies=[j.jid]
jj.execute()

# dbSNP ID annot.
#TODO: Is it really a VEP requirement?
oo=of.replace(".filtered.vcf.gz", ".annotated.vcf.gz")
j=su.bcfAnnotate(of, oo)
j.dependencies=[jj.jid]
j.execute()
